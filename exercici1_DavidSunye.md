# Monkey Island

Com a experts dissenyadors ens han encarregat crear un document on definieix com serà un videojoc.

- Guardem tots els arxius en una carpeta anomenada gitBasic

```
mkdir gitBasic
```

- Utilitem arxius de text pla per guardar la informació.
    - Usarem la nomenclatura markdown
    
### Iniciar repositori

- El primer pas serà crear el repositori git.

```
git init
```

Observem què ha canviat en el directori, ara podem veure com s'ha generat una carpeta .git.

- Podem veure l'estat del nostre repositori

![Imatge d'estat 1](/imatges_exercici1/1.png)

Primer document
- Creem un document de text nou (.txt) amb el nom "videojoc.txt" i escribim:

```
nano videojoc.txt
```

```
# Videojoc
- Aquest document contindrà la definició del joc. 
```


- Al revisar l'estat del repositori. Podem veure que hi ha un fitxer nou.

![Imatge d'estat 2](/imatges_exercici1/2.png)

Afegim els canvis a staging

- Per poder versionar el nou document primer l'hem d'afegir a staging

```
git add videojoc.txt
```

- Revisem l'estat del repositori.

![Imatge d'estat 3](/imatges_exercici1/3.png)

Afegir canvis al repositori

- Confirmem els canvis al repositori git (fent un commit). Afegim com a missatge: "inici document videojoc"

```
git commit -m "inici document videojoc"
```

- Tornem a revisar l'estat del repositori.

![Imatge d'estat 4](/imatges_exercici1/4.png)

Log
Amb git log, podem veure els diferents commits que s'han realitzat al repositori

```
git log
```
![Imatge d'estat 5](/imatges_exercici1/5.png)

### Modificació del document

Seguim definint el videojoc. Comencem a escriure l'argument que tindrà. Decidim fer un videojoc de pirates.

- Afegim el següent text al document

```
# Breu història
Situada en un arxipèlag sense concretar del Carib i en una època indefinida la història comença quan el protagonista, Guybrush Threepwood, arriba a l'illa de Mêlée amb l'aspiració d'esdevenir un pirata.

Allà, busca els líders dels pirates que li demanen superar tres proves per a poder convertir-se en un d'ells, la primera prova consisteix a vèncer la mestra de l'espasa, Carla, en un duel d'insults, la seguna en trobar un tresor enterrat i la tercera en robar una figura de la casa de la governadora de l'illa, Elaine Marley, de qui s'enamora immediatament. 
```

- Afegim els canvis al git

```
git add videojoc.txt
git commit -m "actualizacio videojoc"
```

### Desfer els canvis

- En cas de voler desfer canvis i tornar a versions anteriors, podem fer:

```
git checkout <nomArchiu>
```

### Ignorar fitxers
Volem tenir un fitxer anomenat privat.txt, que no volem que estigui inclós al git.

Creem el fitxer amb el següent contingut
```
Fitxer privat
```
- .gitignore és útil per ignorar arxius generats automàticament o arxius que contenen informació sensible
- Revisem l'estat del repositori.

![Imatge d'estat 10](/imatges_exercici1/10.png)

Podem crear un fitxer anomenat .gitignore on hi llistarem els fitxer que volem amagar al git.

Creem el fitxer .gitignore amb el contingut:

```
privat.txt
```

- Tornem a revisar l'estat del repositori. El fitxer privat.txt ja no apareix en la llista.

![Imatge d'estat 11](/imatges_exercici1/11.png)

>Nota: Si un fitxer ja està sent rastrejat pel git, afegir-lo al .gitignore no tindrà efecte.

### Treballar amb branques

Ja tenim la història acabada, ara volem treballar en la definició del sistema de joc, però volem mantenir la versión actual del document. Per fer-ho farem servir el sistema de branques de git.

- Crearem una branca nova on afegirem el sistema de joc i deixarem l'actual amb l'història acabada.

La branca inicial és diu *master*. Ho comprovem fent *git status*

Per crear una branca nova farem

```
git branch game_system
```

- Per canviar la branca amb la que estem treballant utilitzarem la comanda git checkout:

```
git checkout game_system
```

- Mirem l'estat i comprovem que s'està treballant a la branca game_system

![Imatge d'estat 6](/imatges_exercici1/6.png)

Afegim el següent text al final del document i registrem els canvis al repositori.

```
# Sistema de joc
El sistema de joc consisteix en una pantalla dividida en dues parts principals, la superior conté els gràfics i el cursor, i la inferior una sèrie de menús i botons que permeten realitzar accions i combinar elements que hem anat adquirint durant el joc.
```
```
git add videojoc.txt
git commit -m "sistema de joc"
```

### Modificar arxius

Encara no hem acabat el sistema de joc, però hem detectat un error tipogràfic a l'història. On hauria de posar *segona posa *seguna*.

- Mirem l'estat i miren que no hi hagi cap canvi pendent de registrar.

![Imatge d'estat 7](/imatges_exercici1/7.png)

- Necessitem treballar amb la branca master, així que utilitzem la comanda checkout.

```
git checkout master
```

Un cop a la branca master, ja no veurem la part del sistema de joc.

- Modifiquem seguna per segona, i registrem els canvis al git.

```
git add videojoc.txt
git commit -m "correccio història"
```

### Merge

Donem per finalitzat el sistema de joc, i hem d'aplicar els canvis de la branca game_system a la branca master.

Ens tenim que assegurar que estem a la branca master utilitzant *git status*

![Imatge d'estat 8](/imatges_exercici1/8.png)

Per aplicar els canvis utilitzem:

```
git merge game_system
```
![Imatge de merge](/imatges_exercici1/9.png)

### Eliminar branca

Al ja haver fet merge, la branca game_system ja no la necessitem, així que la esborrarem.

Podem llistar les branques amb:

```
git branch
```

Per eliminar la branca game_system utilitzarem:

```
git branch -d game_system
```
